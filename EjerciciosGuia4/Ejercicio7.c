#include <iostream.h>
#include <cmath.h>
double main(){

    double num;
    double *coeficientes;
    double x;

    cout <<"introduce el numero de coeficientes : ";
    cin>> num;

    //asignacion de memoria
    coeficientes=new double [num];

    if (coeficientes==NULL) 
    {
        cout <<"Error";
    }

    //Llenaremos el vector
    for (int i = 0; i < num; i++)
    {

        cout << "introduce el coeficiente " <<i<< ":";
        cin >> coeficientes[i];
    }

    cout << " Ingrese el valor de X =";

    cin >> x;

    // calculando el valor de p(x)
    
    double acumulador;
    for (int i = 0; i < num; i++)
    {
        acumulador=acumulador+ coeficientes[i] * pow (x, i);
    }
    


    cout << " \nel valor de polinomio en el punto (" << x << ") = " << acumulador << endl;



}